variable "project_name" {
  description = "ACIT 4640 Assignment 3"
}

variable "ami_id" {
  description = "AMI for the server"
}

variable "instance_type" {
  description = "Instance type"
  default = "t2.micro"
}

variable "ssh_key_name" {
  description = "AWS SSH key name"
  default     = "/home/munib/ass3/acit-4640-assignment3/terraform/acit_4640_202330.pem"
}

variable "aws_region" {
  description = "AWS region"
}

variable "availability_zones" {
  description = "AWS availability zone"
  default = ["us-west-2a", "us-west-2b"]
}

variable "subnet_id" {
    description = "Subnet ID"
}

variable "security_group_ids" {
    description = "Security Group ID"
}



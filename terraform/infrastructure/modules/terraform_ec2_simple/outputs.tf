
output "EC2_instance_Public_IP" {
  value = aws_instance.EC2_instance_Public.public_ip
  
}

output "EC2_instance_Private_IP" {
  value = aws_instance.EC2_instance_Private.public_ip
}

output "EC2_instance_Private_DNS" {
  value = aws_instance.EC2_instance_Private.private_ip
}

output "EC2_Instance_Public_IP_2" {
  value = aws_instance.EC2_instance_Private.public_ip
  
}

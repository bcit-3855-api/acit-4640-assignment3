resource "aws_instance" "EC2_instance_Public" {
    ami = var.ami_id
    instance_type = var.instance_type
    key_name = var.ssh_key_name
    subnet_id = var.subnet_id
    security_groups = [var.security_group_ids[0]]
    availability_zone = var.availability_zones
    tags = {
        Name = "EC2_instance"
        Project = var.project_name
    }
}

resource "aws_instance" "EC2_instance_Private" {
    ami = var.ami_id
    instance_type = var.instance_type
    key_name = var.ssh_key_name
    subnet_id = var.subnet_id
    security_groups = [var.security_group_ids[1]]
    availability_zone = var.availability_zones
    tags = {
        Name = "EC2_instance"
        Project = var.project_name
    }
  
}

locals {
  instance_public_dns = aws_instance.EC2_instance_Public.public_dns
  instance_private_dns = aws_instance.EC2_instance_Private.public_dns
  
}

resource "local_file" "inventory" {

  content = <<-EOF
  all:
    vars:
      ansible_user: ubuntu
      ansible_ssh_private_key_file: /home/munibj/acit-4640-assignment3/terraform/infrastructure/acit_4640_202330.pem
    hosts:
      instance1:
        ansible_host: ${aws_instance.EC2_instance_Public.public_dns}
      instance2:
        ansible_host: ${aws_instance.EC2_instance_Private.public_dns}
  EOF

  filename = "/home/munibj/acit-4640-assignment3/ansible/inventory.yaml"
}

resource "local_file" "ansible_config" {

  content = <<-EOT
  [defaults]
  inventory = inventory.yaml
  stdout_callback = debug

  [ssh_connection]
  host_key_checking = False
  ssh_common_args = -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null
  EOT

  filename = "/home/munibj/acit-4640-assignment3/ansible/ansible.cfg"
}

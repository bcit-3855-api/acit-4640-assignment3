variable "aws_region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "project_name" {
  description = "ACIT-4640 Assignment 3"
}

variable "vpc_id" {
  description = "VPC ID"
  
}

variable "public_cidr" {
  description = "Public CIDR"
  default     = "0.0.0.0/0"
  
}

variable "vpc_cidr" {
    description = "VPC CIDR block"
    default = "10.0.0.0/16"
  
}


variable "security_group_name1" {
  description = "Security Group Name"
  default = "ACIT-4640_Security_Group_Frontend"
}

variable "security_group_name2" {
  description = "Security Group Name"
  default = "ACIT-4640_Security_Group_Backend"
}

provider "AWS" {
  region = var.aws_region
  
}
resource "aws_security_group" "security_group1" {
  name = var.security_group_name1
  description = "Security Group for first EC2 instance"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = var.security_group_name1
    Project = var.project_name
  }
}

resource "aws_vpc_security_group_ingress_rule" "SSH_Frontend_Ingress" {
  security_group_id = aws_security_group.security_group1.id
  from_port = 22
  to_port = 22
  ip_protocol = "tcp"
  cidr_ipv4 = var.public_cidr
  tags = {
    Project = var.project_name
    description = "Allow SSH Traffic anywhere"
  }
}

resource "aws_vpc_security_group_ingress_rule" "HTTP_Frontend_Ingress" {
  security_group_id = aws_security_group.security_group1.id
  from_port = 80
  to_port = 80
  ip_protocol = "tcp"
  cidr_ipv4 = var.public_cidr
  tags = {
    Project = var.project_name
    description = "Allow HTTP traffic in"
  }
}

resource "aws_vpc_security_group_egress_rule" "HTTP_Frontend_Egress" {
  security_group_id = aws_security_group.security_group1.id
  ip_protocol = "-1"
  cidr_ipv4 = var.public_cidr
  tags = {
    Project = var.project_name
    description = "Allow all traffic out"
  }
}

resource "aws_security_group" "security_group2" {
  name = var.security_group_name2
  description = "Security Group for second EC2 instance"
  vpc_id = aws_vpc.vpc.id
  tags = {
    name = var.security_group_name2
    Project = var.project_name
  }
}

resource "aws_vpc_security_group_ingress_rule" "SSH_Backend_Ingress" {
  security_group_id = aws_security_group.security_group2.id
  from_port = 22
  to_port = 22
  ip_protocol = "tcp"
  cidr_ipv4 = var.public_cidr
  tags = {
    Project = var.project_name
    description = "Allow SSH traffic in"
  }
}

resource "aws_vpc_security_group_ingress_rule" "Backend_HTTP_Ingress" {
  security_group_id = aws_security_group.security_group2.id
  ip_protocol = "tcp"
  from_port = 80
  to_port = 80
  cidr_ipv4 = var.vpc_cidr
  tags = {
    Project = var.project_name
    description = "Allow HTTP traffic out from VPC only"
  }  
  
}

resource "aws_vpc_security_group_egress_rule" "Backend_HTTP_Egress" {
  security_group_id = aws_security_group.security_group2.id
  ip_protocol = "-1"
  cidr_ipv4 = var.public_cidr
  tags = {
    Project = var.project_name
    description = "Allow all traffic out"
  }
}







output "vpc_id" {
  value = aws_vpc.Assigment3_VPC_1.id
}
output "subnet_id" {
  value = aws_subnet.Assigment3_Subnet_1.id
}

output "internet_gateway_id" {
    value = aws_internet_gateway.Assigment3_Internet_Gateway_1.id
}

output "route_table_id" {
    value = aws_route_table.Assignment3_Route_Table_1.id
}


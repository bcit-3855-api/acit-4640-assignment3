variable "aws_region" {
  description = "The AWS region"
  default = "us-west-2"
  
}

variable "project_name" {
  description = "The name of the project"
  default = "ACIT4640-Assigment3"
  
}
variable "vpc_cidr" {
    description = "VPC CIDR block"
    default = "10.0.0.0/16"
  
}

variable "subnet_cidr" {
    description = "Subnet CIDR block"
    default = "10.0.1.0/24"
  
}

variable "default_route" {
    description = "Default Route"
    default =  "0.0.0.0/0"
}
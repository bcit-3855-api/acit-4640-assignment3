provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "Assigment3_VPC_1" {
    cidr_block =  var.vpc_cidr
    instance_tenancy = "default"
    enable_dns_hostnames = true
    tags = {
        Name = "VPC_1"
        Project = var.project_name
    }

}

resource "aws_subnet" "Assigment3_Subnet_1" {
    vpc_id =  aws.vpc.Assigment3_VPC_1.id
    cidr_block =  var.subnet_cidr
    availability_zone =  "us-west-2a"
    map_public_ip_on_launch = true
    tags = {
        Name = "Subnet_1"
        Project = var.project_name
    }
  
}


resource "aws_internet_gateway" "Assigment3_Internet_Gateway_1" {
    vpc_id = aws_vpc.Assigment3_VPC_1.id
  tags = {
    Name = "Internet_Gateway_1"
    Project = var.project_name
  }
}

resource "aws_route_table" "Assignment3_Route_Table_1" {
    vpc_id = aws_vpc.Assigment3_VPC_1.id
    route {
        cidr_block = var.default_route
        gateway_id = aws_internet_gateway.Assigment3_Internet_Gateway_1.id
    }
    tags = {
        Name = "Route_Table_1"
        Project = var.project_name  
    }
}

resource "aws_route_table_association" "Assignment3_Route_Table_Association_1" {
    subnet_id = aws_subnet.Assigment3_Subnet_1.id
    route_table_id = aws_route_table.Assignment3_Route_Table_1.id
}
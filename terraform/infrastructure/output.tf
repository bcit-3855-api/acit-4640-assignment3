output "ssh_pub_key_file" {
  value = module.ssh_key.pub_key_file
}

output "ssh_priv_key_file" {
  value = module.ssh_key.priv_key_file
}

output "Ec2_Public_IP_1" {
  value = module.ec2.EC2_instance_Public_IP
}

output "EC2_Public_IP_2"{
  value = module.ec2.EC2_Instance_Public_IP_2
}

output "EC2_Private_IP" {
  value = module.ec2.EC2_instance_Private_IP
}


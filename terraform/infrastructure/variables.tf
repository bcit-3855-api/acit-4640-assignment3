variable "aws_region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "project_name" {
  description = "ACIT4640 Assignment3"
}

variable "vpc_cidr" {
  description = "VPC CIDR"
  default     = "10.0.0.0/16"
}

variable "subnet_1" {
  description = "Subnet CIDR"
  default     = "10.0.1.0/24"
}

variable "default_route"{
  description = "Default route"
  default     = "0.0.0.0/0"
}

variable "ssh_key_name"{
  description = "AWS SSH key name"
  default = "acit_4640_202330"
}

variable "ami_id" {
  description = "AMI ID"
}

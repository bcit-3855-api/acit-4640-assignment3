module "vpc" {
  source       = "./modules/terraform_vpc"
  project_name = var.project_name
  vpc_cidr     = var.vpc_cidr
  aws_region   = var.aws_region

}

module "security_groups" {
    source = "./modules/terraform_security_groups"
    project_name = var.project_name
    vpc_id = module.vpc.vpc_id
}

module "ssh_key" {
  source = "./modules/terraform_ssh"
  key_name = var.ssh_key_name
}


module "ec2" {
  source = "./modules/terraform_ec2_simple"
  project_name = var.project_name
  aws_region = var.aws_region
  ami_id = var.ami_id
  subnet_id = module.vpc.subnet_id
  security_group_ids = [module.security_groups.security_group_id, module.security_groups.security_group_id2]
  ssh_key_name = var.ssh_key_name
}
